#!/usr/bin/env python3
'''
Infinite Monkey Theorem

The theorem states that a monkey hitting keys at random on a 
typewriter keyboard for an infinite amount of time will almost
surely type a given text, such as the complete works of William Shakespeare.

Generates random strings that will eventually match
the string specified under match_string variable
'''

import random

alphabet_list = []
match_string = "hello"

'''Generate letters of alphabet'''
def alphabet(letters):
    for letter in letters:
    	alphabet_list.append(letter)

def generate_string(string):
	generated_string = []
	for item in range(len(string)):
		generated_string.append(random.choice(alphabet_list))
	return generated_string

def score(string):
	if "".join(string) == match_string:
		print("".join(string))
		exit()
	else:
		'''
		This compares the index and values of match_string with 
		the index and values of the random string generated
		'''
		current_string = []
		for index, letter in enumerate(match_string):
			index_value = index, letter
			for index1, letter1 in enumerate(string):
				index_value1 = index1, letter1
				if index_value == index_value1:
					current_string.append(letter[0])
	return current_string

def results():
	string_number = 0
	best_string = []
	while True:
		new_string = generate_string(match_string)
		current_score = score(new_string)
		if len(current_score) > len(best_string):
			best_string = current_score
		string_number = string_number + 1
		if string_number % 100000 == 0:  #Print every 100000 entries
		    percentage = len(best_string) / len(match_string) * 100
		    print("%s %d%%" % ("".join(best_string), int(percentage)))

if __name__ == "__main__":

    alphabet("abcdefghijklmnopqrstuvwxyz ")
    print(results())

    