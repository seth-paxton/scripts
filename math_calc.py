#!/usr/bin/env python3
'''
Math Calculators
Equations that come in handy
Seth Paxton v0.1
v0.1 - Inital script, added percentage calculator
'''

import sys

def percentage_change(original_value, new_value):
	subtract_values = original_value - new_value
	percent = subtract_values/original_value
	result = percent * 100
	return result


if __name__ == "__main__":
    try:
	    sys.argv[1] == percentage_change
	    original_value = int(sys.argv[2])
	    new_value = int(sys.argv[3])
	    print("Difference: {}".format(
	    	    percentage_change(original_value, new_value)))
    except IndexError:
        print("Please specify percentage_change and values (i.e. 20 5)")
        exit(1)
    except ValueError:
    	print("Specify numbers only")
    	exit(1)
   