#/usr/bin/env python

import datetime, random, sys
'''
v1 - output phrases based on day of week. Fun with inheritence. Added PEP8 too
v2 - Added exceptions
'''

class File:
    '''Add the contents of a file into a list'''

    fortunes = []

    def fortune_db(self, filename):
        try:
            self.filename = open(filename, 'r')
        except IOError:
            print('File "{0}" not found').format(filename)
            sys.exit(1)
        else:
            for fortune in self.filename:
                fortune = fortune.split('/n')
                self.fortunes.append(fortune)
            return self.fortunes

class Fortune:
    '''Public interface to display fortunes'''

    def __init__(self, fortunes):
        self.current_day = datetime.datetime.today().weekday()
        self.fortunes = fortunes
        self.length_fortunes = len(fortunes) - 1

class Phrases(Fortune):
    '''This could have been a method within Forturne'''

    def picker(self, days_of_week):
        self.days_of_week = days_of_week
        if self.current_day in self.days_of_week:
    	    self.random_fortune = self.fortunes[random.randint(0,self.length_fortunes)]
            return self.random_fortune
        else:
        	sys.exit(1)


if __name__ == "__main__":
    
    '''
    Days of the week represented by datetime
    0 = Monday
    1 = Tuesday
    2 = Wednesday
    3 = Thursday
    4 = Friday
    5 = Saturday
    6 = Sunday
    
    Below is two ways to interact with the Fortune public interface
    '''

    '''(1) Manually adding entries to Fortune'''
    days = [0,3,6]
    funny_fortunes = Phrases(["Do something funny", "You are an idiot", \
    	                      "another zinger"])
    print funny_fortunes.picker(days) 

    '''(2) Having a file as input to Fortune'''
    Fileobject = File()
    db = Fileobject.fortune_db('fortunes.txt')
    funny_fortunes = Phrases(db)
    print '\n'.join(funny_fortunes.picker(days))