#!/usr/bin/env python3
'''
Simple Alerting Program
Seeks to end of a file and alerts on matching string. 
Practicing the observer pattern
'''
import sys 

class Contacts(list):

    def add_contact(self, contact):
        self.append(contact)

    def contacts(self, event):
        for contact in self:
            contact.alert_contacts(event) 

class FileWatcher(Contacts):

    def __init__(self, logname):
    	self.logname = logname

    def search_string(self, string):
        self.string = string
        '''Expandability in the future'''

    def tailfile(self, find):
        self.find = self.search_string(find)
        with open(self.logname) as log:
    	    log.seek(0, 2) # Go to the end of the file.
    	    while True:
                [self.contacts(lines)
                            for lines in log if find in lines] 
class Alert:

    def __init__(self, name):
        self.name = name

    def alert_contacts(self, event):
        print(event)
        print(self.name)
        '''This module could be used to syslog or email in the future'''

if __name__ == "__main__":

    try:
	    filename = sys.argv[1]
    except IndexError:
	    print("Please specify valid filename")
	    exit(1)

    '''Order here matters'''    
    log_file = FileWatcher(filename)
    seth = Alert('Seth')

    log_file.add_contact(seth)
    log_string = log_file.tailfile('pizza')
    

